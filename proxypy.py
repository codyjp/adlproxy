""" Module for acting as a proxy for fetching contents of a url """
import urllib

import urllib2
import json
import urlparse
import re
import copy

import cookielib

import requests

def _validateUrl(urlstr):
    pattern = re.compile(
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|'  # ...or ipv4
            r'\[?[A-F0-9]*:[A-F0-9:]+\]?)'  # ...or ipv6
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)

    protocol = re.match(r'^(\w+)://',urlstr)

    # if a protocol is specified make sure its only http or https
    if (protocol != None) and not(bool(re.match(r'(?:http)s?',protocol.groups()[0]))):
        return False

    return bool(re.search(pattern,urlstr))

def get(request, session):
    try:
        session
    except NameError:
        print "New session, no session passed"
        session = requests.Session()

    """ Builds and returns a JSON reply of all information and requested data """
    args = dict(urlparse.parse_qsl(request.query_string))

    reply = {}
    reply["headers"] = {}
    reply["status"] = {}

    params = {}
    params = copy.deepcopy(args)
    if "url" in params:
        params.pop("url", None)

    print params

    if "url" in args and _validateUrl(args["url"]):
        reply["status"]["url"] = args["url"]

        if not args["url"].startswith("http://"):
            args["url"] = "http://"+args["url"]

        headers = { "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "Host":"library.artstor.org"}
        # , "Connection":"keep-alive"

        try:
            response = session.get(args["url"], headers=headers, params=params, cookies=request.cookies)
            # data=request.form,
            return response

        except (urllib2.HTTPError, urllib2.URLError) as e:
            reply["status"]["reason"] = str(e.reason)
            reply["content"] = None
            reply["status"]["http_code"] = e.code if hasattr(e,'code') else 0
    else:
        reply["content"] = None
        reply["status"]["http_code"] = 400
        reply["status"]["reason"] = "The url parameter value is missing or invalid"

    # Attach callback to reply if jsonp request
    if "callback" in args:
        return "{0}({1})".format(args["callback"], json.dumps(reply))

    return json.dumps(reply)

def post(form, session):
    try:
        session
    except NameError:
        session = requests.Session()

    # args = dict(urlparse.parse_qsl(qstring))

    args = dict(urlparse.parse_qsl(form.query_string))

    #url = qstring;

    reply = {}
    reply["headers"] = {}
    reply["status"] = {}

    # if "headers"
    # reply["headers"] = form.headers

    if "url" in args and _validateUrl(args["url"]):
        if not args["url"].startswith("http://"):
            args["url"] = "http://"+args["url"]
        reply["status"]["url"] = args["url"]

        # , "x-requested-with" : "XMLHttpRequest"
        headers = { "Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "Host":"library.artstor.org" }

        # req = urllib2.Request(args["url"], data, headers)
        try:
            response = session.post(args["url"], headers=headers, data=form.form, allow_redirects=False)
            return response

        except ( requests.exceptions.HTTPError,  requests.exceptions.URLRequired) as e:
            reply["status"]["http_code"] = e.code if hasattr(e,'code') else 0
            # reply["status"]["reason"] = str(e.reason)
            reply["content"] = e.read()

        # reply["origin_request"] = str(form)
    else:
        reply["content"] = None
        reply["status"]["http_code"] = 400
        reply["status"]["reason"] = "The url parameter value is missing or invalid"

    # Attach callback to reply if jsonp request
    if "callback" in args:
        return "{0}({1})".format(args["callback"], json.dumps(reply))
    #the_page = response.read()
    return json.dumps(reply)
