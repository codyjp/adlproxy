from flask import Flask, request, Response, render_template
import json
import sys
import Cookie
# import cookielib
import requests
sys.path.append("..")
import proxypy

app = Flask(__name__)

def add_cors_headers(response):
    print request
    response.headers['Access-Control-Allow-Origin'] = 'http://localhost'
    if "Origin" in request.headers:
        response.headers['Access-Control-Allow-Origin'] = request.headers['Origin']
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    if request.method == 'OPTIONS':
        response.headers['Access-Control-Allow-Methods'] = 'DELETE, GET, POST, PUT'
        headers = request.headers.get('Access-Control-Request-Headers')
        if headers:
            response.headers['Access-Control-Allow-Headers'] = headers
    return response
app.after_request(add_cors_headers)

@app.before_request
def option_autoreply():
    """ Always reply 200 on OPTIONS request """

    if request.method == 'OPTIONS':
        resp = app.make_default_options_response()

        headers = None
        if 'ACCESS_CONTROL_REQUEST_HEADERS' in request.headers:
            headers = request.headers['ACCESS_CONTROL_REQUEST_HEADERS']

        h = resp.headers

        # Allow the origin which made the XHR
        h['Access-Control-Allow-Origin'] = request.headers['Origin']
        # Allow the actual method
        h['Access-Control-Allow-Methods'] = request.headers['Access-Control-Request-Method']
        # Allow for 10 seconds
        h['Access-Control-Max-Age'] = "10"

        # We also keep current headers
        if headers is not None:
            h['Access-Control-Allow-Headers'] = headers

        return resp

@app.route("/")
def index():
    return render_template("index.html")

# http://rocky-cliffs-9470.herokuapp.com/api?url=library.artstor.org/library/secure/institutions/
@app.route("/api", methods=['GET','POST'])
def crossdomainapi():
    print request
    if request.method == 'POST':
        session = requests.Session()
        reply = proxypy.post(request,session)

        # returnObj.headers['Expires'] = 'Thu, 01 Jan 1970 00:00:00 GMT'
        print 'return headers'
        print reply.headers
        print reply.cookies

        if reply.headers['location'].find('library.artstor.org/library/secure/userinfo') > 0:
            print 'trying returned redirect'
            # followUpRequest['query_string'] = reply.headers['location']
            newReply = session.get(reply.headers['location'])
            print newReply
            print newReply.headers

        if newReply:
            returnObj = Response(newReply,status=200,mimetype='text/html')
        else:
            returnObj = Response(reply,status=200,mimetype='text/html')

        returnObj.headers['Keep-Alive'] = 'timeout=5, max=999'
        returnObj.headers['Cache-Control'] = 'no-cache'
        returnObj.headers['Pragma'] = 'no-cache'

        if "JSESSIONID" in reply.cookies:
            print reply.cookies["JSESSIONID"]
            C2 = Cookie.SimpleCookie()
            C2["JSESSIONID"] = reply.cookies["JSESSIONID"]
            C2["JSESSIONID"]["path"] = "/"
            returnObj.headers.add_header("Set-Cookie", C2.output(header=''))

        if "ARTSTOR_HASHED_REM_ME" in reply.cookies:
            print reply.cookies["ARTSTOR_HASHED_REM_ME"]
            C3 = Cookie.SimpleCookie()
            C3["ARTSTOR_HASHED_REM_ME"] = reply.cookies["ARTSTOR_HASHED_REM_ME"]
            C3["ARTSTOR_HASHED_REM_ME"]["path"] = "/"
            returnObj.headers.add_header("Set-Cookie", C3.output(header=''))

        if "ORA_WX_SESSION" in reply.cookies:
            print reply.cookies["ORA_WX_SESSION"]
            C4 = Cookie.SimpleCookie()
            C4["ORA_WX_SESSION"] = reply.cookies["ORA_WX_SESSION"]
            C4["ORA_WX_SESSION"]["path"] = "/"
            returnObj.headers.add_header("Set-Cookie", C4.output(header=''))

        if "TIMEOUTC" in reply.cookies:
            print reply.cookies["TIMEOUTC"]
            C5 = Cookie.SimpleCookie()
            C5["TIMEOUTC"] = reply.cookies["TIMEOUTC"]
            C5["TIMEOUTC"]["path"] = "/"
            returnObj.headers.add_header("Set-Cookie", C5.output(header=''))

        if "email" in reply.cookies:
            print reply.cookies["email"]
            C6 = Cookie.SimpleCookie()
            C6["email"] = reply.cookies["email"]
            C6["email"]["path"] = "/"
            returnObj.headers.add_header("Set-Cookie", C6.output(header=''))

        return returnObj
    else:
        session = requests.Session()
        reply = proxypy.get(request, session)
        return Response(reply,status=200,mimetype='application/json')

if __name__ == "__main__":
    app.run(debug=True)
